const axios = require('axios');
const cheerio = require('cheerio');
const express = require('express');

const PORT = 8000;
const URL = 'https://www.theguardian.com/international';

const app = express();

app.get('/', (req, res) => {
    res.send('Hello world');
});

axios(URL)
    .then(response => {
        const html = response.data;
        const $ = cheerio.load(html);

        const articles = []

        $('.fc-item__title', html).each(function () {
            const title = $(this).text();
            const url = $(this).find('a').attr('href');
            articles.push({ title, url });
        });

        console.log(articles);
    }).catch(err => console.error(err));

app.listen(PORT, () => { console.log(`Server running on localhost:${PORT}`) });